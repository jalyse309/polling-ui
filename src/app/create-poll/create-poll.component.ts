import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { PaloozaService } from '../palooza-services.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-poll',
  templateUrl: './create-poll.component.html',
  styleUrls: ['./create-poll.component.css']
})
export class CreatePollComponent implements OnInit {
  pollForm;
  private pollsArray: any;

  constructor(private paloozaService: PaloozaService, private formBuilder: FormBuilder,  private router: Router) {      // injecting so it may be used
    this.pollForm = this.formBuilder.group(         // control(),group(), and array() are 3 factories supported with formbuilder.
      {          // key values. the value will be an array
        question: '',
        options: this.formBuilder.array([])
      });
  }
  // tslint:disable-next-line:typedef
  ngOnInit() {  // right from the bat it should direct me to 2 options
    this.addOption();
    this.addOption();
    console.log(this.pollForm);
  }

  options(): FormArray {
    return this.pollForm.get("options") as FormArray;
  }

  private newOption(): FormGroup { // creating a restriction
    return this.formBuilder.group({
      value: '',
    }, [
      Validators.required, // perform synchronous validation. class implementations can be minLength || maxLength
      Validators.minLength(10)
    ]);
  }

  // tslint:disable-next-line:typedef
  addOption() { // adding options
    this.options().push(this.newOption());
  }

  // tslint:disable-next-line:typedef
  removeOption(i: number) { // removing options from a specific place/index
    this.options().removeAt(i);
  }

  // tslint:disable-next-line:typedef
  checkIfOptionsNull() { // If the person did not type in the form, disregard it. Do not submit.
    let isEmpty = false;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.options().length; i++){
      console.log();
      const val = this.options().controls[i].value.value; // Adding cross-validation to reactive forms

      if (val === '' && val.length >= 30){
        isEmpty = true;
        break;
      }
    }
    return isEmpty;
  }

  gotoPoll(): void{
    console.log("method actually started");
    this.paloozaService.getPolls().subscribe(
      data => {
        this.pollsArray = data;
        console.log(data);
        console.log(this.pollsArray);
      },
      err => {
        console.error(err);
      },
      () => {
        console.log("Got all polls");
        this.goToPollPart2();
      }
    );
  }

  goToPollPart2(): void{
    const latestPollId = this.pollsArray.length;

    const path = "/polls/" + (latestPollId);
    console.log(path);
    this.router.navigate([path]);
  }

  // tslint:disable-next-line:typedef
  onSubmit(newPollData: any) {
    // Process checkout data here
    // new +
    // this.pollForm.controls.options.patchValue();
    this.paloozaService.createPoll(newPollData).subscribe(
      data => {
        console.warn('Your order has been submitted', newPollData);
      },
      err => {
        console.error(err);
      },
      () => {
        console.log("all done.");
      }
    );
    this.pollForm.reset();
    this.gotoPoll();
  }
}
