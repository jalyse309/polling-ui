import { Component, OnInit } from '@angular/core';
import {PaloozaService} from '../palooza-services.service';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.css']
})
export class VoteComponent implements OnInit {
  poll;
  pollForm = new FormGroup({
    vote: new FormControl("", Validators.required)
  });
  validMessage;
  pollId = this.route.snapshot.params.id;

  constructor(private paloozaService: PaloozaService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.getPollByID(this.route.snapshot.params.id);
  }

  // tslint:disable-next-line:typedef
  getPollByID(id: number){
    this.paloozaService.getPollByID(id).subscribe(
      data => {
        this.poll = data;
      },
      err => console.error(err),
      () => console.log("Poll loaded successfully.")
    );
  }

  onSubmit(): void{
    const pollForm = this.pollForm;
    const poll = this.poll;
    const pollId = this.pollId;
    const paloozaService = this.paloozaService;
    if (pollForm.invalid){
      this.validMessage = "You must select an option before you can submit.";
      return;
    }
    else{
      poll.options.forEach((currentOption) => {
        if (pollForm.get("vote").value === currentOption.value){
          const result = {
            option: {
              id: currentOption.id,
              value: currentOption.value
            }
          };
          paloozaService.castVote(pollId, result).subscribe(
            data => {
              this.validMessage = "Your vote for " + result.option.value + " was successful!";
              console.log("Successfully put in a vote for " + result.option.id + " " + result.option.value);
            },
            error => {
              return Observable.throw(error);
            },
            () => {
              this.gotoResult();
            }
          );
        }
      });
      }
    }

    gotoResult(): void{
      this.router.navigate(['polls/' + this.pollId + '/results']);
    }
  }
