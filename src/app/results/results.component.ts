import { Component, OnInit } from '@angular/core';
import { PaloozaService } from '../palooza-services.service';
import { ActivatedRoute } from '@angular/router';
import {AfterViewInit} from '@angular/core';
import {Poll} from '../models/poll';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  poll;
  // letting us get the id from the URL
  pollId = this.route.snapshot.params.id;
  result;
  resultsArray: string[] = [];
  resultsReady = false;
  disabled = false;


  constructor(private paloozaService: PaloozaService, private route: ActivatedRoute) {
  }

  // these methods will happen right away
  ngOnInit(): void {
    this.setPollByID(this.pollId);
    this.setResult(this.pollId);
    // this.totalResults();
  }


  // ngAfterViewInit(): void {
  //   this.totalResults();
  // }
  // uses poll id to get poll info from database
  // stores it in poll object
  setPollByID(pollId: number): void{
    this.paloozaService.getPollByID(pollId).subscribe(
      data => {
        this.poll = data;
      },
      err => {
        console.error(err);
      },
      () => {
        console.log("Poll loaded successfully!");
      }
    );
  }
  // tslint:disable-next-line:typedef
  setResult(pollId: number): void{
    this.paloozaService.computeResults(pollId).subscribe(
      data => {
        this.result = data;
      },
      err => {
        console.error(err);
      },
      () => {
        console.log("The result is loaded!");
      }
    );
  }

  displayResults(): void{
    this.poll.options.forEach(currentOption => {
      this.result.results.forEach(currentResult => {
        console.log("The result is loaded successfully!");
        this.totalResults();
      }
    );
  });
  }

  // displayResults(): void{
  //   this.poll.options.forEach(currentOption => {
  //     this.result.results.forEach(currentResult => {
  //       console.log("The result is loaded successfully!");
  //     }
  //   );
  // });
  // }

  totalResults(): void{
    const optionIds = [];
    for (const currentOption of this.poll.options){
      for (const currentResult of this.result.results){
        optionIds[optionIds.length] = currentResult.optionId;
        if (currentOption.id === currentResult.optionId){
          this.resultsArray[this.resultsArray.length] =
          "Option: " + currentOption.value + " | Votes: " + currentResult.count;
        }
      }
      if (!optionIds.includes(currentOption.id)) {
        this.resultsArray[this.resultsArray.length] =
          "Option: " + currentOption.value + " | Votes: 0";
      }
      this.resultsReady = true;
    }
  }
}
