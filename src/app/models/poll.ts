import { Poll_Options } from './poll_options';


export class Poll {

    public id: number;
    public question: string;
    public options: Poll_Options;

}
