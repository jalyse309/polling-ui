import { Poll_Options } from './poll_options';


export class Vote {

    public id: number;
    public pollOptions: Poll_Options;
}