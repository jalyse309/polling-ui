import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Poll} from './models/poll';
import {Observable} from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({"Content-Type": "application/json"})
};

@Injectable({
  providedIn: 'root'
})
export class PaloozaService {

  constructor(private http: HttpClient) { }

  // tslint:disable-next-line:typedef
  getPolls(){
    return this.http.get("/server/polls");
  }

  // tslint:disable-next-line:typedef
  getPollByID(id: number): Observable<any>{
    return this.http.get("/server/polls/" + id);
  }
  createPoll(poll: any): Observable<any> {
    return this.http.post(`/server/polls/`, poll);
  }

  // tslint:disable-next-line:typedef
  castVote(pollId: number, body){
    const jsonBody = JSON.stringify(body);
    return this.http.post("/server/polls/" + pollId + "/votes", jsonBody, httpOptions);
  }

  // tslint:disable-next-line:typedef
  getResults(pollId: number){
    return this.http.get("/server/polls/" + pollId + "/votes");
  }
  computeResults(pollId: number): Observable<any>{
    return this.http.get("/server/polls/" + pollId + "/computeresult");
  }
}
