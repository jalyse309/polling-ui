import { TestBed } from '@angular/core/testing';

import { PaloozaService } from './palooza-services.service';

describe('PaloozaServicesService', () => {
  let service: PaloozaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaloozaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
